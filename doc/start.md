#  快速开始

    import std.sync.*
    import std.time.*
    import quartz.org.quartz.*
    import quartz.org.quartz.utils.*

    class MyJob <: Job {
        public func execute(context: JobExecutionContext): Unit {
            println("MyJob execute called ${DateTime.now()}")
        }
    }

    func sample1(): Unit {

        let scheduler = StdSchedulerFactory.getDefaultScheduler()

        let jobClass = newJobClass({ => MyJob() }, "MyJob" )
        let job1 = JobBuilder.newJob(jobClass).withIdentity("job1").build();
        let trig1 = TriggerBuilder.newTrigger().withIdentity("trig1").withSchedule(SimpleScheduleBuilder.repeatSecondlyForever(1)).build()
        scheduler.scheduleJob(job1, trig1);

        scheduler.start()

        sleep( Duration.second * 10)

        scheduler.shutdown(true)
    }

    输出:

    MyJob execute called 2023-08-06T18:29:45+08:00
    MyJob execute called 2023-08-06T18:29:46+08:00
    ...
    MyJob execute called 2023-08-06T18:29:54+08:00
    MyJob execute called 2023-08-06T18:29:55+08:00

[返回](../README.md)