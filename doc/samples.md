# 示例程序

所有示例程序放在doc/samples目录下

- sample1  使用SimpleTrigger [示例1](samples/sample1/sample1.cj)
- sample2  使用CronTrigger  [示例2](samples/sample2/sample2.cj)
- sample3  无需实现Job子类来定义job  [示例3](samples/sample3/sample3.cj)
- sample4  给Job和Trigger传参数, 使用包 org.sample4  [示例4](samples/sample4/sample4.cj)
- sample5  通过Properties对象配置启动参数, 调度器配置为多个线程  [示例5](samples/sample5/sample5.cj)
- sample6  通过quartz.properties配置启动参数, 调度器配置为多个线程  [示例6](samples/sample6/sample6.cj) [配置文件](samples/sample6/quartz6.properties)
- sample7  同时调度多个job  [示例7](samples/sample7/sample7.cj)
- sample8  使用监听器  [示例8](samples/sample8/sample8.cj) [配置文件](samples/sample8/quartz8.properties)
- sample9  每隔1天运行一次，排除所有节假日不要调度 [示例9](samples/sample9/sample9.cj)
  
[返回](../README.md)
