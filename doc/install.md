# 集成quartz4cj

1.  下载quartz4cj源码

        将quartz4cj源码clone到本地，和要集成quartz4cj的项目放在同一级目录下
        如果不是在同一级目录下，后面配置path_option的地方也需要做相应调整

2.  编译quartz4cj源码

        进入quartz4cj根目录，运行cjpm clean && cjpm build编译源码
        源码编译成功后，输出文件在 target/release/quartz 目录下，此目录下有所有 .cjo .a文件
            libquartz.org.quartz.a
            libquartz.org.quartz.listeners.a
            libquartz.org.quartz.utils.a
            quartz.org.quartz.cjo
            quartz.org.quartz.listeners.cjo
            quartz.org.quartz.utils.cjo
        注意：当前分支的quartz4cj版本仅支持 cangjie 0.56.4 版本

3.  集成quartz4cj组件

        进入项目根目录，修改 module.json, 在path_option中增加quartz4cj库路径
            "package_requires": {
                "path_option": [ "../quartz4cj/target/release/quartz" ],
                "package_option": {}
            },

4.  源码中加入quartz4cj调用并运行

            加入import：

            from quartz import org.quartz.*
            from quartz import org.quartz.utils.*

            增加调用：

            let jobKey = JobKey("job1")  // org.quartz 包下的类
            println("jobKey=${jobKey}")
            let triggerKey = TriggerKey("trig1")
            println("triggerKey=${triggerKey}")
            let p = Properties() // org.quartz.utils 包下的类
            p.setProperty("a", "1")
            println("a=${p.getProperty("a")}")

            输出为：

            jobKey=DEFAULT.job1
            triggerKey=DEFAULT.trig1
            a=Some(1)

[返回](../README.md)