# quartz.properties 可配置项

配置参数可以直接通过Properties对象来配置， 也可以放在 quartz.properties (也可用其它文件名) 文件中

    org.quartz.threadPool.threadCount=10  线程数
    org.quartz.scheduler.instanceName=test_timer6  调度器名称

    org.quartz.context.key.k10=v10  调度器的上下文参数1, 会传递到JobExecutionContext对象里
    org.quartz.context.key.k11=v11  调度器的上下文参数2
    org.quartz.context.key.k12=v12  调度器的上下文参数3

    org.quartz.jobListener.ja.class=org.sample8.TestJobListener1  JobListener插件1类名
    org.quartz.jobListener.jb.class=org.sample8.TestJobListener2  JobListener插件2类名, 实现BeanSupport接口, 支持参数p3,p4
    org.quartz.jobListener.jb.p3=v3  JobListener插件2的配置参数
    org.quartz.jobListener.jb.p4=v4  JobListener插件2的配置参数

    org.quartz.triggerListener.ta.class=org.sample8.TestTriggerListener1  TriggerListener插件1类名
    org.quartz.triggerListener.tb.class=org.sample8.TestTriggerListener2  TriggerListener插件2类名, 实现BeanSupport接口, 支持参数p5,p6
    org.quartz.triggerListener.tb.p5=v5  TriggerListener插件2的配置参数
    org.quartz.triggerListener.tb.p6=v6  TriggerListener插件2的配置参数

[返回](../README.md)