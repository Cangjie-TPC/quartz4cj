# 常用API

- [Job接口定义](../src/org/quartz/job.cj)  提供接口形式和函数形式
- [创建JobClass](../src/org/quartz/jobclass.cj) 创建JobClass, 模拟java里的 job class
- [定义JobDetail](../src/org/quartz/jobbuilder.cj) 通过JobBuilder创建JobDetail
- [定义Trigger](../src/org/quartz/triggerbuilder.cj) 通过TriggerBuilder创建Trigger, 需要使用 withSchedule 确定具体使用 SimpleTrigger 还是 CronTrigger
- [创建SimpleTrigger](../src/org/quartz/simpleschedulebuilder.cj) 创建SimpleTrigger对象
- [创建CronTrigger](../src/org/quartz/cronschedulebuilder.cj) 创建CronTrigger对象
- [传递Job参数](../src/org/quartz/jobdatamap.cj) 可通过JobDataMap对象和Job进行通讯
- [Job执行上下文](../src/org/quartz/jobexecutioncontext.cj) 可获取到Job执行时的上下文信息
- [排除掉触发器上的某些时间不调度](../src/org/quartz/calendar.cj) 可根据需要在触发器上排除某些特殊时间，如节假日，周末等
  * [按星期中哪几天来排除](../src/org/quartz/impl_calendar_weeklycalendar.cj) 
  * [按月份中的哪几天来排除](../src/org/quartz/impl_calendar_monthlycalendar.cj) 
  * [指定具体节假日来排除](../src/org/quartz/impl_calendar_holidaycalendar.cj) 
  * [按开始结束时分秒来排除](../src/org/quartz/impl_calendar_dailycalendar.cj) 
  * [按月日来排除](../src/org/quartz/impl_calendar_annualcalendar.cj) 
  * [按cron表达式来排除](../src/org/quartz/impl_calendar_croncalendar.cj) 
- [调度器](../src/org/quartz/scheduler.cj) 调度器接口, 无需关心具体实现类
- [调度器工厂类](../src/org/quartz/impl_stdschedulerfactory.cj) 通过调度器工厂来获取调度器

- 通过properties传递插件信息给调度器工厂
  * [Properties类](../src/org/quartz/utils/properties.cj) Properties类，支持从配置文件里读配置  
  * [Supplier接口](../src/org/quartz/supplier.cj) 如果希望通过properties配置动态加载插件/监听器，每个插件/监听器需提供一个Supplier函数
  * [BeanSupport接口](../src/org/quartz/suppliers.cj) 如果希望通过properties动态配置插件/监听器属性，需实现BeanSupport接口
  * [Suppliers类](../src/org/quartz/suppliers.cj) 插件/监听器存储仓库，供调度器工厂查找
  * [监听器管理器](../src/org/quartz/listenermanager.cj) 通过调度器可获取到监听器调度器，可不通过properties而是完全通过代码调用此接口来增加监听器

- 监听器
  * [JobListner监听器](../src/org/quartz/joblistener.cj) 应用程序可定制自己的JobListner监听器
  * [TriggerListner监听器](../src/org/quartz/triggerlistener.cj) 应用程序可定制自己的TriggerListner监听器
  * [SchedulerListner监听器](../src/org/quartz/schedulerlistener.cj) 应用程序可定制自己的SchedulerListner监听器, 只能通过监听器管理器来增加

- [Cron表达式](../src/org/quartz/cronexpression.cj) Cron表达式类，可独立使用用来验证cron表达式是否正确

[返回](../README.md)
