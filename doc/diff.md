# 仓颉版本和java版本的差异点

仓颉版本基于java quartz 2.4.0版本进行开发

- 仓颉版本仅支持RAMJobStore, 不支持将job存储到数据库中
- 对spi下的一些接口，仓颉版本目前启动时只加载默认实现，暂不支持加载扩展实现，包括：JobStore, ThreadPool, ThreadExecutor, JobFactory, InstanceIdGenerator
- 仓颉版本通过JobClass类模拟java版本里的Class, 对外API接口基本不变
- 仓颉版本通过BeanSupport接口来模拟java版本里的反射，用于给插件动态设置属性
- 由于仓颉语言不支持包之间循环依赖，仓颉版本大部分代码都统一使用org.quartz包，通过文件名命名和原有类的位置对应
- 仓颉版本的日志目前集成的是cj jdk里的SimpleLogger, 目前仅 支持输出信息到控制台 
- 

[返回](../README.md)