# quartz4cj

## 介绍

<font color="#dd0000">实现JAVA生态中quartz的主要功能</font>。 quartz4cj是一个功能丰富的开源作业调度库，几乎可以集成在任何<font color="#dd00dd">仓颉</font>应用程序中，从最小的独立应用程序到最大的电子商务系统。主要用来执行定时任务，如：定时发送信息、定时生成报表等等，可通过触发器设置作业定时运行规则，控制作业的运行时间。代码参考：<a href="https://github.com/quartz-scheduler/quartz"><font color="#0000dd">quartz官方文档</font></a>

## 软件架构

quartz4cj框架主要核心组件包括调度器、触发器、作业。调度器作为作业的总指挥，触发器作为作业的操作者，作业为应用的功能模块。其关系如下图所示：
![图片](doc/images/arc.png)

其中Job为作业的接口，为任务调度的对象；JobDetail用来描述Job的实现类及其它相关的静态信息；Trigger做为作业的定时管理工具，一个Trigger只能对应一个作业实例，而一个作业实例可对应多个触发器；Scheduler做为定时任务容器，是quartz最上层的东西，它提携了所有触发器和作业，使它们协调工作，每个Scheduler都存有JobDetail和Trigger的注册信息，一个Scheduler中可以注册多个JobDetail和多个Trigger。


## 集成quartz4cj

[集成quartz4cj](doc/install.md) 当前版本仅支持 0.56.4 版本

## 用户手册

[1 快速开始](doc/start.md)

[2 示例程序](doc/samples.md)

[3 常用API](doc/api.md)

[4 仓颉版本和java版本的差异点](doc/diff.md)

[5 quartz.properties 可配置项](doc/properties.md)

## 开源协议

Apache License Version 2.0

## 技术支持和建议

quartz4cj项目由上海赛可出行科技服务有限公司架构团队实现并维护。 欢迎大家试用，并提供宝贵的意见和建议。技术支持和意见反馈请提Issue。

