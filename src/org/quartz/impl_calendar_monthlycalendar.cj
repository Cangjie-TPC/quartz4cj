/*
 * 版权所有 (c) 上海赛可出行科技服务有限公司 2023-2023
 * 
 * Created by ranpengfei on 2023/08/04
 */
package quartz.org.quartz

import std.time.*


/*
 * 基于月份来排除时间, 可以设置哪几个月要排除
 * 
 */
public class MonthlyCalendar <: BaseCalendar {
 
    private static let MAX_DAYS_IN_MONTH = 31;

    private var excludeDays = Array<Bool>(MAX_DAYS_IN_MONTH,repeat:false);

    var excludeAll = false;

    /*
    * 创建对象
    */
    public MonthlyCalendar( baseCalendar: ?Calendar,  timeZone: ?TimeZone ) {
        super(baseCalendar, timeZone);
        excludeAll = areAllDaysExcluded();
    }

    /*
    * 创建对象
    */
    public init() {
        this(None, None);
    }

    /*
    * 创建对象
    */
    public init( baseCalendar: Calendar) {
        this(baseCalendar, None);
    }

    /*
    * 创建对象
    */
    public init( timeZone: TimeZone) {
        this(None, timeZone);
    }

    /*
    * 复制对象
    */
    public func clone(): Calendar {
        let newObj = MonthlyCalendar(this.baseCalendar,this.timeZone)
        newObj.setDescription(this.description)
        newObj.excludeDays = excludeDays.clone()
        newObj.excludeAll = areAllDaysExcluded();
        return newObj
    }

    /*
    * 获取哪些月份要排除
    */
    public func getDaysExcluded() : Array<Bool> {
        return excludeDays;
    }

    /*
    * 判断月份是否要排除
    */
    public func isDayExcluded( day: Int64 ): Bool  {
        if ((day < 1) || (day > MAX_DAYS_IN_MONTH)) {
            throw IllegalArgumentException(  "The day parameter must be in the range of 1 to ${MAX_DAYS_IN_MONTH}" );
        }

        return excludeDays[day - 1];
    }

    /*
    * 批量设置月份是否要排除
    */
    public func setDaysExcluded(days: Array<Bool>) : Unit  {
        if (days.size != MAX_DAYS_IN_MONTH) {
            throw IllegalArgumentException("The days parameter must have a length of at least ${MAX_DAYS_IN_MONTH} elements.");
        }

        excludeDays = days;
        excludeAll = areAllDaysExcluded();
    }

    /*
    * 设置月份是否要排除
    */
    public func setDayExcluded(day: Int64, exclude: Bool ): Unit  {
        if ((day < 1) || (day > MAX_DAYS_IN_MONTH)) {
            throw IllegalArgumentException( "The day parameter must be in the range of 1 to ${MAX_DAYS_IN_MONTH}");
        }

        excludeDays[day - 1] = exclude;
        excludeAll = areAllDaysExcluded();
    }

    /*
    * 是否所有月份都要排除
    */
    public func areAllDaysExcluded() : Bool {
        for ( i in  1 ..= MAX_DAYS_IN_MONTH ) {
            if (!isDayExcluded(i)) {
                return false;
            }
        }

        return true;
    }

    /*
    *  时间戳是否在日历的有效时间范围内
    */
    public func isTimeIncluded(timeStamp: Int64) : Bool {
        if (excludeAll) {
            return false;
        }

        if (!super.isTimeIncluded(timeStamp)) { 
            return false; 
        }

        let dt = toDateTime(timeStamp)
        return !(isDayExcluded(dt.dayOfMonth));
    }

    /*
    *  获取下一个有效时间
    */
    public func getNextIncludedTime(timeStamp0: Int64 ) :Int64 {

        var  timeStamp = timeStamp0

        if (excludeAll) {
            return 0;
        }

        let baseTime = super.getNextIncludedTime(timeStamp);
        if ((baseTime > 0) && (baseTime > timeStamp)) {
            timeStamp = baseTime;
        }

        let dt = toDateTime(timeStamp)
        var day = dt.dayOfMonth

        if (!isDayExcluded(day)) {
            return timeStamp; // return the original value
        }

        while (isDayExcluded(day) ) {
            timeStamp += 24*60*60*1000
            let dt = toDateTime(timeStamp)
            day = dt.dayOfMonth
        }

        return timeStamp;
    }

}
