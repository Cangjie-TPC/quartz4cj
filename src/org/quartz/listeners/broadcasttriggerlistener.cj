/*
 * 版权所有 (c) 上海赛可出行科技服务有限公司 2023-2023
 * 
 * Created by ranpengfei on 2023/08/04
 */
package quartz.org.quartz.listeners

import quartz.org.quartz.*
import quartz.org.quartz.utils.*

import std.collection.*

/*
 * TriggerListener广播监听器, 此监听器可广播事件给一组 TriggerListener
 */
public class BroadcastTriggerListener  <: TriggerListener { 
   
    private let name :String 
    private let listeners : ArrayList<TriggerListener>

    public BroadcastTriggerListener( name: String) {
        this.name = name;
        listeners = ArrayList<TriggerListener>();
    }

    public init( name: String, ls: Collection<TriggerListener> ) {
        this(name);
        this.listeners.appendAll(ls);
    }

    public func  getName(): String {
        return name;
    }

    public func addListener( listener: TriggerListener) :Unit {
        listeners.append(listener);
    }

    public func removeListener( listener: TriggerListener) : Unit {
        removeListener(listener.getName())
    }

    public func removeListener( listenerName: String) : Unit  {
        listeners.removeIf( { l => l.getName() == listenerName } )
    }

    public func getListeners() :  Collection<TriggerListener>{
        return ArrayList<TriggerListener>(listeners);
    }


    public func triggerFired( trigger: Trigger,  context: JobExecutionContext) : Unit {
        for(jl in listeners) {
            jl.triggerFired(trigger,context);
        }
    }

    public func vetoJobExecution(trigger: Trigger,  context: JobExecutionContext) : Bool {
        for(jl in listeners) {
            if(jl.vetoJobExecution(trigger, context)) {
                return true;
            }
        }
        return false;
    }

    public func triggerMisfired( trigger: Trigger)  : Unit  {
        for(jl in listeners) {
            jl.triggerMisfired(trigger);
        }
    }

    public func triggerComplete(trigger: Trigger,  context: JobExecutionContext, 
             triggerInstructionCode: CompletedExecutionInstruction)   : Unit {

        for(jl in listeners) {
            jl.triggerComplete(trigger, context, triggerInstructionCode);
        }
    }

}
