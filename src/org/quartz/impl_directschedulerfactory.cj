/*
 * 版权所有 (c) 上海赛可出行科技服务有限公司 2023-2023
 * 
 * Created by ranpengfei on 2023/08/04
 */
package quartz.org.quartz

import std.collection.*
import quartz.org.quartz.utils.*

/*
 *  通过代码来初始化调度器的工厂类
 */
public class DirectSchedulerFactory  {

    private static let log = LoggerFactory.getLogger("org.quartz.DirectSchedulerFactory")

    public static let DEFAULT_INSTANCE_ID = "SIMPLE_NON_CLUSTERED";
    public static  let DEFAULT_SCHEDULER_NAME = "SimpleQuartzScheduler";
    private static let DEFAULT_THREAD_EXECUTOR = DefaultThreadExecutor();
    private static let DEFAULT_BATCH_MAX_SIZE = 1;
    private static let DEFAULT_BATCH_TIME_WINDOW = 0;

    private static var instance = DirectSchedulerFactory();

    public static func getInstance(): DirectSchedulerFactory {
        return instance;
    }

    private var initialized = false;

    private DirectSchedulerFactory() {
    }

    /*
    *  根据线程数来创建调度器，其它使用默认值
    */
    public func createVolatileScheduler(maxThreads: Int64) : Unit {
        let threadPool = SimpleThreadPool(maxThreads);
        let jobStore = RAMJobStore();
        this.createScheduler(threadPool: threadPool, jobStore : jobStore);
    }

    /*
    *  通过参数来创建调度器
    */
    public func createScheduler( threadPool!: ThreadPool,
                                 jobStore! : JobStore, 
                                 schedulerName! : String = DEFAULT_SCHEDULER_NAME,
                                 schedulerInstanceId! : String = DEFAULT_INSTANCE_ID,  
                                 threadExecutor! : ThreadExecutor = DEFAULT_THREAD_EXECUTOR,
                                 schedulerPluginMap! : HashMap<String, SchedulerPlugin> = HashMap(),
                                 idleWaitTime! : Int64 = -1,
                                 maxBatchSize!: Int64 = DEFAULT_BATCH_MAX_SIZE, 
                                 batchTimeWindow!: Int64 = DEFAULT_BATCH_TIME_WINDOW) : Unit {


        let jobFactory = PropertySettingJobFactory() 
        let jrsf = StdJobRunShellFactory();
 
        threadPool.setInstanceName(schedulerName)
        threadPool.setInstanceId(schedulerInstanceId) 
        
        let qrs = QuartzSchedulerResources();

        let threadName = schedulerName+ "_QuartzSchedulerThread";

        qrs.setName(schedulerName);
        qrs.setThreadName(threadName);
        qrs.setInstanceId(schedulerInstanceId);

        qrs.setJobRunShellFactory(jrsf);
        qrs.setThreadPool(threadPool);
        qrs.setThreadExecutor(threadExecutor);
        qrs.setJobStore(jobStore);
        qrs.setMaxBatchSize(maxBatchSize);
        qrs.setBatchTimeWindow(batchTimeWindow);
        
        threadExecutor.initialize();
        threadPool.initialize();

        for (p in schedulerPluginMap.values() ) {
            qrs.addSchedulerPlugin(p );
        }

        let qs = QuartzSchedulerImpl(qrs, jobFactory, idleWaitTime);

        jobStore.setInstanceName(schedulerName)
        jobStore.setInstanceId(schedulerInstanceId) 
        jobStore.initialize(qs.getSchedulerSignaler());

        let scheduler = StdScheduler(qs);

        jrsf.initialize(scheduler);

        qs.initialize();
        
        for( (key,p) in schedulerPluginMap ) {
            p.initialize(key,scheduler)
        }

        log.info("Quartz scheduler '" + scheduler.getSchedulerName());
        log.info("Quartz scheduler version: " + qs.getVersion());

        let schedRep = SchedulerRepository.getInstance();

        qs.addNoGCObject(schedRep); // prevents the repository from being garbage collected

        schedRep.bind(scheduler);
        
        initialized = true;
    }

    /*
    * 获取调度器
    * 
    * 必须先调用过createScheduler方法
    * 获取调度器后会自动绑定到调度器存储库
    */
    public func getScheduler() : Scheduler {
        if (!initialized) {
            throw SchedulerException( "you must call createScheduler methods before calling getScheduler()");
        }

        return getScheduler(DEFAULT_SCHEDULER_NAME).getOrThrow();
    }

    /*
    * 获取指定名称的调度器， 若尚未创建，则返回None
    */
    public func getScheduler( schedName: String)  : ?Scheduler  {
        return SchedulerRepository.getInstance().lookup(schedName);
    }

    /*
    * 获取所有调度器
    */
    public func getAllSchedulers() : Collection<Scheduler> {
        return SchedulerRepository.getInstance().lookupAll();
    }
}
