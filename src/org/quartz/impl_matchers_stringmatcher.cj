/*
 * 版权所有 (c) 上海赛可出行科技服务有限公司 2023-2023
 * 
 * Created by ranpengfei on 2023/08/04
 */
package quartz.org.quartz

import quartz.org.quartz.utils.*

public enum StringOperatorName <: Hashable & Equatable<StringOperatorName> {
    |EQUALS|STARTS_WITH|ENDS_WITH|CONTAINS|ANYTHING

    public func evaluate(value:String, compareTo:String) : Bool {
        match( this ) {
            case EQUALS => value == compareTo;
            case STARTS_WITH => value.startsWith(compareTo);
            case ENDS_WITH => value.endsWith(compareTo);
            case CONTAINS => value.contains(compareTo);
            case ANYTHING => true;
        }
    }

    public func ordinal() : Int64 {
        match( this ) {
            case EQUALS => 1;
            case STARTS_WITH => 2;
            case ENDS_WITH => 3;
            case CONTAINS => 4;
            case ANYTHING => 5;
        }
    }

    public func hashCode() : Int64 {
        return ordinal()
    }

    public operator func ==(other: StringOperatorName) : Bool {
        return this.ordinal() == other.ordinal()
    }

    public operator func !=(other: StringOperatorName) : Bool {
        return this.ordinal() != other.ordinal()
    }
}

public abstract class StringMatcher<T> <: Matcher<T> where T <: Key<T> {
       
    protected StringMatcher(let compareTo: String, let compareWith: StringOperatorName) {
    }

    public open func getValue(key: T) : String
    
    public func isMatch(key: T) : Bool {
        return compareWith.evaluate(getValue(key), compareTo);
    }

    public func hashCode() : Int64 {
        let prime = 31;
        var result = 1;
        result = prime * result + compareTo.hashCode();
        result = prime * result + compareWith.hashCode();
        return result;
    }
 
    public operator func ==(obj: Matcher<T>) : Bool {
        match( obj ) {
            case other: StringMatcher<T> => 
                return this.compareTo == other.compareTo &&
                    this.compareWith == other.compareWith 
            case _ => false
        }
    }

    public operator func !=(obj: Matcher<T>) : Bool {
        match( obj ) {
            case other: StringMatcher<T> => 
                return this.compareTo != other.compareTo ||
                    this.compareWith != other.compareWith 
            case _ => true
        }
    }

    public func getCompareToValue() : String {
        return compareTo;
    }

    public func getCompareWithOperator() : StringOperatorName {
        return compareWith;
    }

}
