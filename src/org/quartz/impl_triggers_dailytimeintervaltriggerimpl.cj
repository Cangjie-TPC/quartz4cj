/*
 * 版权所有 (c) 上海赛可出行科技服务有限公司 2023-2023
 * 
 * Created by xujian on 2023/08/04
 */
package quartz.org.quartz

import std.time.*
import std.collection.*

import quartz.org.quartz.utils.*

 public class DailyTimeIntervalTriggerImpl <: AbstractTrigger<DailyTimeIntervalTrigger> & DailyTimeIntervalTrigger {

    private var startTime: DateTime = DateTime.now()
    private var endTime: ?DateTime = None
    private var nextFireTime: ?DateTime = None
    private var previousFireTime: ?DateTime = None
    private var repeatCount: Int64 = REPEAT_INDEFINITELY
    private var repeatInterval: Int64 = 1
    private var repeatIntervalUnit: IntervalUnit = IntervalUnit.MINUTE
    private var daysOfWeek: Set<Int64> = HashSet<Int64>()
    private var startTimeOfDay: TimeOfDay = TimeOfDay(0,0,0)
    private var endTimeOfDay: ?TimeOfDay = None
    private var timesTriggered: Int64 = 0
    private var complete: Bool = false    

    public DailyTimeIntervalTriggerImpl() {
        super()
    }

    public init(name: String, startTimeOfDay: TimeOfDay, endTimeOfDay: TimeOfDay, intervalUnit: IntervalUnit,  repeatInterval: Int64) {
        this(name, "", startTimeOfDay, endTimeOfDay, intervalUnit, repeatInterval)
    }

    public init(name: String, group: String, startTimeOfDay: TimeOfDay, endTimeOfDay: TimeOfDay, intervalUnit: IntervalUnit,  repeatInterval: Int64) {
        super(name, group)
        setStartTime(DateTime.now())
        setRepeatIntervalUnit(intervalUnit)
        setRepeatInterval(repeatInterval)
        setStartTimeOfDay(startTimeOfDay)
        setEndTimeOfDay(endTimeOfDay)
    }  

    public init(name: String, startTime: DateTime, endTime: DateTime, startTimeOfDay: TimeOfDay, endTimeOfDay: TimeOfDay, intervalUnit: IntervalUnit,  repeatInterval: Int64) {
        this(name, "", startTime, endTime, startTimeOfDay, endTimeOfDay, intervalUnit, repeatInterval)
    }

    public init(name: String, group: String, startTime: DateTime, endTime: DateTime, startTimeOfDay: TimeOfDay, endTimeOfDay: TimeOfDay, intervalUnit: IntervalUnit,  repeatInterval: Int64) {
        super(name, group)

        setStartTime(startTime)
        setEndTime(endTime)
        setRepeatIntervalUnit(intervalUnit)
        setRepeatInterval(repeatInterval)
        setStartTimeOfDay(startTimeOfDay)
        setEndTimeOfDay(endTimeOfDay)
    }   

    public init(name: String, group: String, jobName: String, jobGroup: String, startTime: DateTime, endTime: DateTime, startTimeOfDay: TimeOfDay, endTimeOfDay: TimeOfDay, intervalUnit: IntervalUnit,  repeatInterval: Int64) {
        super(name, group, jobName, jobGroup)

        setStartTime(startTime)
        setEndTime(endTime)
        setRepeatIntervalUnit(intervalUnit)
        setRepeatInterval(repeatInterval)
        setStartTimeOfDay(startTimeOfDay)
        setEndTimeOfDay(endTimeOfDay)
    }
    
    public override func getStartTime(): DateTime {
        return startTime
    }    

    public override func setStartTime(startTime: DateTime): Unit {
        var eTime = getEndTime()
        if (eTime.isPresent() && eTime.getOrThrow() < startTime) {
            throw IllegalArgumentException("End time cannot be before start time")
        }

        this.startTime = startTime
    }

    public override func getEndTime(): ?DateTime {
        return endTime
    }    

    public override func setEndTime(endTime: ?DateTime): Unit {
        var sTime = getStartTime()
        if (endTime != None && sTime > endTime.getOrThrow()) {
            throw IllegalArgumentException("End time cannot be before start time")
        }

        this.endTime = endTime
    }    

    public func getRepeatIntervalUnit(): IntervalUnit {
        return repeatIntervalUnit
    }    

    public func setRepeatIntervalUnit(intervalUnit: IntervalUnit): Unit {
        // quartz 原有代码这里有 bug, 判断的不是入参, 已修复
        if (!((intervalUnit == IntervalUnit.SECOND || 
                intervalUnit == IntervalUnit.MINUTE || 
                intervalUnit == IntervalUnit.HOUR))) {
            throw IllegalArgumentException("Invalid repeat IntervalUnit (must be SECOND, MINUTE or HOUR).")
        }
        this.repeatIntervalUnit = intervalUnit
    }    

    public func getRepeatInterval(): Int64 {
        return repeatInterval
    }    

    public func setRepeatInterval(repeatInterval: Int64): Unit {
        if (repeatInterval < 0) {
            throw IllegalArgumentException("Repeat interval must be >= 1")
        }

        this.repeatInterval = repeatInterval
    }    

    public func getTimesTriggered() {
        return timesTriggered
    }    

    public func setTimesTriggered(timesTriggered: Int64): Unit {
        this.timesTriggered = timesTriggered
    }    

    protected func validateMisfireInstruction(misfireInstruction: Int64): Bool {
        return misfireInstruction >= MISFIRE_INSTRUCTION_IGNORE_MISFIRE_POLICY && misfireInstruction <= MISFIRE_INSTRUCTION_DO_NOTHING
    }    

    public override func updateAfterMisfire(cal: ?Calendar): Unit {
        var instr = getMisfireInstruction()
        if(instr == MISFIRE_INSTRUCTION_IGNORE_MISFIRE_POLICY) {
            return
        }

        if (instr == MISFIRE_INSTRUCTION_SMART_POLICY) {
            instr = MISFIRE_INSTRUCTION_FIRE_ONCE_NOW
        }

        if (instr == MISFIRE_INSTRUCTION_DO_NOTHING) {
            var newFireTime = getFireTimeAfter(DateTime.now())
            while (newFireTime.isPresent() && cal.isPresent() && !cal.getOrThrow().isTimeIncluded(newFireTime.getOrThrow().getTime())) {
                newFireTime = getFireTimeAfter(newFireTime)
            }
            setNextFireTime(newFireTime)
        } else if (instr == MISFIRE_INSTRUCTION_FIRE_ONCE_NOW) { 
            // fire once now...
            setNextFireTime(DateTime.now())
            // the new fire time afterward will magically preserve the original  
            // time of day for firing for day/week/month interval triggers, 
            // because of the way getFireTimeAfter() works - in its always restarting
            // computation from the start time.
        }
    }    

    public override func triggered(calendar: ?Calendar): Unit {
        timesTriggered++
        previousFireTime = nextFireTime
        nextFireTime = getFireTimeAfter(nextFireTime)

        while (nextFireTime.isPresent() && calendar.isPresent() && !calendar.getOrThrow().isTimeIncluded(nextFireTime.getOrThrow().getTime())) {
            nextFireTime = getFireTimeAfter(nextFireTime)
            if(nextFireTime == None) {
                break
            }
            
            //avoid infinite loop
            if (nextFireTime.getOrThrow().year > TimeUtil.YEAR_TO_GIVEUP_SCHEDULING_AT) {
                nextFireTime = None
            }
        }
        
        if (nextFireTime == None) {
            complete = true
        }
    }    

    public override func updateWithNewCalendar(calendar: ?Calendar, misfireThreshold: Int64): Unit {
        nextFireTime = getFireTimeAfter(previousFireTime)
        if (nextFireTime == None || !calendar.isPresent()) {
            return
        }
        
        let now = DateTime.now()
        while (nextFireTime != None && !calendar.getOrThrow().isTimeIncluded(nextFireTime.getOrThrow().getTime())) {
            nextFireTime = getFireTimeAfter(nextFireTime)
            if(nextFireTime == None) {
                break
            }
            
            //avoid infinite loop
            if (nextFireTime.getOrThrow().year > TimeUtil.YEAR_TO_GIVEUP_SCHEDULING_AT) {
                nextFireTime = None
            }

            if(nextFireTime != None && nextFireTime.getOrThrow() < now) {
                var diff = now.getTime() - nextFireTime.getOrThrow().getTime()
                if(diff >= misfireThreshold) {
                    nextFireTime = getFireTimeAfter(nextFireTime)
                }
            }
        }
    }    

    public override func computeFirstFireTime(calendar: ?Calendar): ?DateTime {
        var dateTime = getStartTime().addSeconds(-1)
        nextFireTime = getFireTimeAfter(dateTime)
      
        // Check calendar for date-time exclusion
        while (nextFireTime != None && calendar.isPresent() && !calendar.getOrThrow().isTimeIncluded(nextFireTime.getOrThrow().getTime())) {
            nextFireTime = getFireTimeAfter(nextFireTime)
            if(nextFireTime == None) {
                break
            }
        
            //avoid infinite loop
            if (nextFireTime.getOrThrow().year  > TimeUtil.YEAR_TO_GIVEUP_SCHEDULING_AT) {
                return None
            }
        }
        
        return nextFireTime
    }    

    private func createCalendarTime(dateTime: DateTime): Calendars {
        var cal = Calendars.getInstance()
        cal.setTime(dateTime)
        return cal
    }    

    public override func getNextFireTime(): ?DateTime {
        return nextFireTime
    }    

    public override func getPreviousFireTime(): ?DateTime {
        return previousFireTime
    }    

    public func setNextFireTime(nextFireTime: ?DateTime): Unit {
        this.nextFireTime = nextFireTime
    }
    
    public func setPreviousFireTime(previousFireTime: DateTime) {
        this.previousFireTime = previousFireTime
    }    

    public override func getFireTimeAfter(afterTime0: ?DateTime): ?DateTime {
        // Check if trigger has completed or not.
        if (complete) {
            return None
        }
        
        // Check repeatCount limit
        if (repeatCount != REPEAT_INDEFINITELY && timesTriggered > repeatCount) {
            return None
        }
      
        // a. Increment afterTime by a second, so that we are comparing against a time after it!
        var afterTime = ( afterTime0 ?? DateTime.now() ).addSeconds(1)
         
        // make sure afterTime is at least startTime
        if(afterTime < startTime) {
            afterTime = startTime
        }

        // b.Check to see if afterTime is after endTimeOfDay or not. If yes, then we need to advance to next day as well.
        var afterTimePastEndTimeOfDay = false
        if (endTimeOfDay.isPresent()) {
            afterTimePastEndTimeOfDay = afterTime > endTimeOfDay.getOrThrow().getTimeOfDayForDate(afterTime)
        }

        // c. now we need to move move to the next valid day of week if either: 
        // the given time is past the end time of day, or given time is not on a valid day of week
        var fireTime0 = advanceToNextDayOfWeekIfNecessary(afterTime, afterTimePastEndTimeOfDay)
        var fireTime = fireTime0 ?? return None

        // d. Calculate and save fireTimeEndDate variable for later use
        var fireTimeEndDate:DateTime
        if (!endTimeOfDay.isPresent()) {
            fireTimeEndDate = TimeOfDay(23, 59, 59).getTimeOfDayForDate(fireTime)
        } else {
            fireTimeEndDate = endTimeOfDay.getOrThrow().getTimeOfDayForDate(fireTime)
        }

        // e. Check fireTime against startTime or startTimeOfDay to see which go first.
        var fireTimeStartDate = startTimeOfDay.getTimeOfDayForDate(fireTime)
        if (fireTime < fireTimeStartDate) {
          return fireTimeStartDate
        } 
        
        // f. Continue to calculate the fireTime by incremental unit of intervals.
        // recall that if fireTime was less that fireTimeStartDate, we didn't get this far
        var fireMillis: Int64 = fireTime.getTime()
        var startMillis: Int64 = fireTimeStartDate.getTime()
        var secondsAfterStart: Int64 = (fireMillis - startMillis) / 1000
        var repeatLong: Int64 = getRepeatInterval()
        var repeatUnit = getRepeatIntervalUnit()

        var sTime = createCalendarTime(fireTimeStartDate)

        if(repeatUnit == IntervalUnit.SECOND) {
            var jumpCount: Int64 = secondsAfterStart / repeatLong
            if(secondsAfterStart % repeatLong != 0) {
                jumpCount++
            }
            sTime.add(Calendars.SECOND, getRepeatInterval() * jumpCount)
            fireTime = sTime.getTime()
        } else if(repeatUnit == IntervalUnit.MINUTE) {
            var jumpCount: Int64 = secondsAfterStart / (repeatLong * 60)
            if(secondsAfterStart % (repeatLong * 60) != 0) {
                jumpCount++
            }
            sTime.add(Calendars.MINUTE, getRepeatInterval() * jumpCount)
            fireTime = sTime.getTime()
        } else if(repeatUnit == IntervalUnit.HOUR) {
            var jumpCount: Int64 = secondsAfterStart / (repeatLong * 60 * 60)
            if(secondsAfterStart % (repeatLong * 60 * 60) != 0){
                jumpCount++
            }
            sTime.add(Calendars.HOUR_OF_DAY, getRepeatInterval() * jumpCount)
            fireTime = sTime.getTime()
        }
        
        // g. Ensure this new fireTime is within the day, or else we need to advance to next day.
        if (fireTime > fireTimeEndDate) {
          let fireTime1 = advanceToNextDayOfWeekIfNecessary(fireTime, isSameDay(fireTime, fireTimeEndDate))
          fireTime = fireTime1 ?? return None
          // make sure we hit the startTimeOfDay on the new day
          fireTime = startTimeOfDay.getTimeOfDayForDate(fireTime)
        }
    
        // quartz 原有代码这里有 bug, endTime不能是最后一次触发时间，已修复
        let eTime = getEndTime()
        if ( eTime.isPresent()  && fireTime >= eTime.getOrThrow() ) {
            return None
        }

        // i. Return calculated fireTime.
        return fireTime
    }    

    private func isSameDay(d1: DateTime, d2: DateTime): Bool {
      return d1.year == d2.year && d1.month == d2.month && d1.dayOfMonth == d2.dayOfMonth
    }  

    private func advanceToNextDayOfWeekIfNecessary(fireTime0: DateTime, forceToAdvanceNextDay: Bool): ?DateTime {
        // a. Advance or adjust to next dayOfWeek if need to first, starting next day with startTimeOfDay.
        var fireTime = fireTime0
        var sTimeOfDay = getStartTimeOfDay()
        var fireTimeStartDate = sTimeOfDay.getTimeOfDayForDate(fireTime)      
        var fireTimeStartDateCal = createCalendarTime(fireTimeStartDate)          
        var dayOfWeekOfFireTime = fireTimeStartDateCal.get(Calendars.DAY_OF_WEEK)
        
        // b2. We need to advance to another day if isAfterTimePassEndTimeOfDay is true, or dayOfWeek is not set.
        var daysOfWeekToFire = getDaysOfWeek()
        if (forceToAdvanceNextDay || !daysOfWeekToFire.contains(dayOfWeekOfFireTime)) {
          // Advance one day at a time until next available date.
          for(_ in 1..=7) {
            fireTimeStartDateCal.add(Calendars.DAY_OF_MONTH, 1)
            dayOfWeekOfFireTime = fireTimeStartDateCal.get(Calendars.DAY_OF_WEEK)
            if (daysOfWeekToFire.contains(dayOfWeekOfFireTime)) {
              fireTime = fireTimeStartDateCal.getTime()
              break
            }
          }
        }
        
        // Check fireTime not pass the endTime
        var eTime = getEndTime()
        if (eTime.isPresent() && fireTime.getTime() >= eTime.getOrThrow().getTime()) {  // quartz 原有代码这里有 bug， endTime不能是最后一次触发时间
            return None
        }

        return fireTime
    }      

    public override func getFinalFireTime(): ?DateTime {

        if (complete ) {
            return None
        }

        var eTime = getEndTime() ?? return None 

        // We have an endTime, we still need to check to see if there is a endTimeOfDay if that's applicable.

        if ( let Some(et) <- endTimeOfDay ) {
            var endTimeOfDayDate = et.getTimeOfDayForDate(eTime)
            if (eTime > endTimeOfDayDate) { // quartz 原有代码这里有 bug, 应该是 >, 已修复
                eTime = endTimeOfDayDate
            }
        }

        // quartz 原有代码这里有 bug, 代码没写完， 直接把 endTime 当成 finalFireTime, 不做修复

        return eTime
    }    

    public override func mayFireAgain(): Bool {
        return getNextFireTime().isPresent()
    }    

    public override func validate(): Unit {
        super.validate()
        
        if (!((repeatIntervalUnit == IntervalUnit.SECOND) || 
                repeatIntervalUnit == IntervalUnit.MINUTE ||
                repeatIntervalUnit == IntervalUnit.HOUR)) {
            throw SchedulerException("Invalid repeat IntervalUnit (must be SECOND, MINUTE or HOUR).")
        }
        if (repeatInterval < 1) {
            throw SchedulerException("Repeat Interval cannot be zero.")
        }
        
        // Ensure interval does not exceed 24 hours
        var secondsInHour = 24 * 60 * 60
        if (repeatIntervalUnit == IntervalUnit.SECOND && repeatInterval > secondsInHour) {
            throw SchedulerException("repeatInterval can not exceed 24 hours (" + secondsInHour.toString() + " seconds). Given " + repeatInterval.toString())
        }
        if (repeatIntervalUnit == IntervalUnit.MINUTE && repeatInterval > secondsInHour / 60) {
            throw SchedulerException("repeatInterval can not exceed 24 hours (" + (secondsInHour / 60).toString() + " minutes). Given " + repeatInterval.toString())
        }
        if (repeatIntervalUnit == IntervalUnit.HOUR && repeatInterval > 24 ) {
            throw SchedulerException("repeatInterval can not exceed 24 hours. Given " + repeatInterval.toString() + " hours.")
        }        
        
        // Ensure timeOfDay is in order.
        // NOTE: We allow startTimeOfDay to be set equal to endTimeOfDay so the repeatCount can be set to 1.
        if ( endTimeOfDay.isPresent() && getStartTimeOfDay() > endTimeOfDay.getOrThrow() ) {
            throw SchedulerException("StartTimeOfDay " + startTimeOfDay.toString() + " should not come after endTimeOfDay " + endTimeOfDay.toString())
        }
    }     

    public func getDaysOfWeek(): Set<Int64> {
        if (daysOfWeek.isEmpty()) {
            daysOfWeek = DailyTimeIntervalScheduleBuilder.ALL_DAYS_OF_THE_WEEK
        }
        return daysOfWeek
    }    

    public func setDaysOfWeek(daysOfWeek0: Set<Int64>): Unit {
        if(daysOfWeek0.size == 0)  {
            throw IllegalArgumentException("DaysOfWeek set must contain at least one day.")
        }

        this.daysOfWeek = daysOfWeek0
    }    

    public func getStartTimeOfDay(): TimeOfDay {
        return startTimeOfDay
    }

    public func setStartTimeOfDay(startTimeOfDay: TimeOfDay): Unit {
        var eTime = getEndTimeOfDay()
        if ( eTime.isPresent() && eTime.getOrThrow().before(startTimeOfDay)) {
            throw IllegalArgumentException("End time of day cannot be before start time of day") 
        }

        this.startTimeOfDay = startTimeOfDay
    }

    public func getEndTimeOfDay(): ?TimeOfDay {
        return endTimeOfDay
    }    

    public func setEndTimeOfDay(endTimeOfDay: TimeOfDay): Unit {
        var sTime = getStartTimeOfDay()
        if (endTimeOfDay.before(sTime)) {
            throw IllegalArgumentException("End time of day cannot be before start time of day")
        }
        this.endTimeOfDay = endTimeOfDay
    }    

    public func getScheduleBuilder(): ScheduleBuilder {
        let cb = DailyTimeIntervalScheduleBuilder.dailyTimeIntervalSchedule().
                withInterval(getRepeatInterval(), getRepeatIntervalUnit()).
                onDaysOfTheWeek(getDaysOfWeek()).
                startingDailyAt(getStartTimeOfDay()).
                endingDailyAt(getEndTimeOfDay());
            
        let v = getMisfireInstruction()
        match(v) {
            case v where v == MISFIRE_INSTRUCTION_DO_NOTHING => cb.withMisfireHandlingInstructionDoNothing()
            case v where v == MISFIRE_INSTRUCTION_FIRE_ONCE_NOW => cb.withMisfireHandlingInstructionFireAndProceed()
            case _ => ()
        }
        
        return cb
    }    

    public func getRepeatCount(): Int64 {  
        return repeatCount
    }    

    public func setRepeatCount(repeatCount: Int64): Unit {
        if (repeatCount < 0 && repeatCount != REPEAT_INDEFINITELY) {
            throw IllegalArgumentException("Repeat count must be >= 0, use the constant REPEAT_INDEFINITELY for infinite.")
        }

        this.repeatCount = repeatCount
    }    

    public operator func ==(o: OperableTrigger): Bool {
        return getKey() == o.getKey()
    }

    public func compare(o: OperableTrigger): Ordering {
        return this.getKey().compare(o.getKey())
    }

    public operator func !=(o:  OperableTrigger): Bool {
        return !(this == o)
    }

    public operator func <(o:  OperableTrigger): Bool {
        return this.compare(o) == Ordering.LT
    }

    public operator func >(o:  OperableTrigger): Bool {
        return this.compare(o) == Ordering.GT
    }

    public operator func <=(o:  OperableTrigger): Bool {
        return match (this.compare(o)) {
            case LT | EQ => true
            case _ => false
        }
    }

    public operator func >=(o:  OperableTrigger): Bool {
        return match (this.compare(o)) {
            case GT | EQ => true
            case _ => false
        }
    }     

    public func getTriggerBuilder(): TriggerBuilder<DailyTimeIntervalTrigger> {
        return TriggerBuilder<DailyTimeIntervalTrigger>.newTrigger()
            .forJob(getJobKey())
            .modifiedByCalendar(getCalendarName())
            .usingJobData(getJobDataMap())
            .withDescription(getDescription())
            .endAt(getEndTime().getOrThrow())
            .withIdentity(getKey())
            .withPriority(getPriority())
            .startAt(getStartTime())
            .withSchedule(getScheduleBuilder()) 
    }       


    public override func clone(): OperableTrigger {
        let t = DailyTimeIntervalTriggerImpl()

        t.name = name
        t.group = group
        t.jobName = jobName
        t.jobGroup = jobGroup
        t.description = description
        t.jobDataMap = JobDataMap(jobDataMap)  
        t.volatility = volatility 
        t.calendarName = calendarName
        t.fireInstanceId = fireInstanceId
        t.misfireInstruction = misfireInstruction
        t.priority = priority
        t.key = key

        t.startTime = startTime
        t.endTime = endTime
        t.nextFireTime = nextFireTime
        t.previousFireTime = previousFireTime
        t.repeatCount = repeatCount
        t.repeatInterval = repeatInterval
        t.repeatIntervalUnit = repeatIntervalUnit
        t.daysOfWeek = daysOfWeek
        t.startTimeOfDay = startTimeOfDay
        t.endTimeOfDay = endTimeOfDay
        t.timesTriggered = timesTriggered
        t.complete = complete

        return t        
    }      

    public func getClass(): String {
        return "org.quartz.DailyTimeIntervalTriggerImpl"
    }    
 }