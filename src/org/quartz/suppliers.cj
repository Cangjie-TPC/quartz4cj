/*
 * 版权所有 (c) 上海赛可出行科技服务有限公司 2023-2023
 * 
 * Created by ranpengfei on 2023/08/04
 */
package quartz.org.quartz

import std.collection.*
import std.sync.*


/*
  * 用于模拟java的反射
  * 
  * 如果扩展对象实现了 BeanSupport 接口，就可以通过 quartz.properties 进行配置
  */
public interface BeanSupport {

    /*
    * 设置对象属性， key为属性名, value为值
    * 
    * 由扩展类自己实现对属性值的解析(如类型转换)和设置
    */
    func setFieldValue(key:String,value:String):Unit 

}

/*
 * Suppliers 用来存储外部插件对象的创建函数，供 quartz 启动时加载 
 * 
 *     cj 里不支持类和反射操作； 所有插件和监听器必须由调用方事先配置好传给quartz框架
 *     目前支持的对象有: SchedulerPlugin JobListener TriggerListener
 *     
 *     Suppliers 为单例对象
 * 
 */
public class Suppliers {

    private static let inst = createSuppliers()
    
    private static func createSuppliers() {
        return Suppliers()
    }

    /*
    * 获取 Suppliers 单例
    */
    public static func getInstance() : Suppliers {
        return inst;
    }

    private let objects = HashMap<String, Any>()
    private let objectsLock = Monitor()

    private let creators = HashMap<String, Supplier>()

    /*
    * 设置创建函数
    *    className 类名， 建议传扩展类的全路径类名
    *    supplier 创建扩展类对象的方法
    */
    public func setSupplier( className: String, supplier: Supplier ) : Unit  {
        creators.put(className,supplier)
    }

    /*
    * 获取创建函数
    */
    public func getObject( className: String ) : ?Any  {
        synchronized( objectsLock) {
            let objOpt = objects.get(className)
            return objOpt ?? createObject(className)
        }
    }

    private func createObject( className: String ) : ?Any  {
        let supplierOpt = creators.get(className)
        match( supplierOpt ) {
            case Some(supplier) => 
                let obj = supplier()
                objects.put(className,obj)
                return obj
            case _ =>
                return None
        }
    }

    public func clear() : Unit {
        synchronized( objectsLock) {
            objects.clear()
            creators.clear()
        }
    }
}
