/*
 * 版权所有 (c) 上海赛可出行科技服务有限公司 2023-2023
 * 
 * Created by ranpengfei on 2023/08/04
 */
package quartz.org.quartz

import quartz.org.quartz.utils.*

/*
 *  KeyMatcher 根据 key 来筛选 JobKey 或 TriggerKey
 */
public class KeyMatcher<T> <: Matcher<T> where T <: Key<T> {
       
    protected KeyMatcher(let compareTo: T) {
    }

    /*
    * 创建对象: key 是否等于入参 compareTo
    */
    public static func keyEquals(compareTo: T) : KeyMatcher<T> {
        return KeyMatcher(compareTo);
    }

    /*
    * 传入的 JobKey 或 TriggerKey 是否匹配
    */
    public func isMatch(key: T) : Bool {
        return compareTo == key;
    }

    public func getCompareToValue() : T {
        return compareTo;
    }

    public func hashCode() : Int64 {
        let prime = 31;
        var result = 1;
        result = prime * result + compareTo.hashCode();
        return result;
    }
 
    public operator func ==(obj: Matcher<T>) : Bool {
        match( obj ) {
            case other: KeyMatcher<T> => 
                return this.compareTo == other.compareTo
            case _ => false
        }
    }

    public operator func !=(obj: Matcher<T>) : Bool {
        match( obj ) {
            case other: KeyMatcher<T> => 
                return this.compareTo != other.compareTo
            case _ => true
        }
    }

}
