/*
 * 版权所有 (c) 上海赛可出行科技服务有限公司 2023-2023
 * 
 * Created by ranpengfei on 2023/08/04
 */
package quartz.org.quartz

import quartz.org.quartz.utils.*

/*
 *  GroupMatcherMatcher 根据 group 来筛选 JobKey 或 TriggerKey
 */
public class GroupMatcher<T> <: StringMatcher<T> where T <: Key<T> {
   
    protected GroupMatcher(compareTo:String , compareWith: StringOperatorName) {
        super(compareTo, compareWith);
    }
    
    public func getValue(key: T) : String {
        return key.getGroup();
    }

    /*
    * 创建对象: group 是否等于入参  compareTo
    */
    public static func groupEquals(compareTo: String) : GroupMatcher<T> {
        return GroupMatcher<T>(compareTo, StringOperatorName.EQUALS);
    }

    /*
    * 创建对象: group 是否 startsWith 入参  compareTo
    */
    public static func  groupStartsWith(compareTo: String) : GroupMatcher<T> {
        return GroupMatcher<T>(compareTo, StringOperatorName.STARTS_WITH);
    }

    /*
    * 创建对象: group 是否 endsWith 入参  compareTo
    */
    public static func groupEndsWith(compareTo: String) : GroupMatcher<T> {
        return GroupMatcher<T>(compareTo, StringOperatorName.ENDS_WITH);
    }

    /*
    * 创建对象: group 是否 contains 入参  compareTo
    */
    public static func  groupContains(compareTo: String) : GroupMatcher<T>{
        return GroupMatcher<T>(compareTo, StringOperatorName.CONTAINS);
    }

    public static func anyGroup(): GroupMatcher<T> {
        return GroupMatcher<T>("", StringOperatorName.ANYTHING);
    }

}
