/*
 * 版权所有 (c) 上海赛可出行科技服务有限公司 2023-2023
 * 
 * Created by xujian on 2023/08/04
 */
package quartz.org.quartz

/* 
* 基于间隔时间的触发器的 Builder 类
*/
public class SimpleScheduleBuilder <: ScheduleBuilder {
    var interval = 0
    var repeatCount = 0
    var misfireInstruction = Trigger.MISFIRE_INSTRUCTION_SMART_POLICY

    /*
    * 创建对象
    */
    public SimpleScheduleBuilder() {
    }

    /*
    * 静态方法创建对象
    */
    public static func simpleSchedule(): SimpleScheduleBuilder {
        return SimpleScheduleBuilder()
    }

    /*
    * 静态方法创建对象, 每分钟重复一次，次数不限制
    */
    public static func repeatMinutelyForever(): SimpleScheduleBuilder {
        return simpleSchedule()
            .withIntervalInMinutes(1)
            .repeatForever()
    }

    /*
    * 静态方法创建对象, 每n分钟重复一次，次数不限制
    */
    public static func repeatMinutelyForever(minutes: Int64): SimpleScheduleBuilder {
        return simpleSchedule()
            .withIntervalInMinutes(minutes)
            .repeatForever()
    }

    /*
    * 静态方法创建对象, 每秒重复一次，次数不限制
    */
    public static func repeatSecondlyForever(): SimpleScheduleBuilder {
        return simpleSchedule()
            .withIntervalInSeconds(1)
            .repeatForever()
    }

    /*
    * 静态方法创建对象, 每n秒重复一次，次数不限制
    */
    public static func repeatSecondlyForever(seconds: Int64): SimpleScheduleBuilder {
        return simpleSchedule()
            .withIntervalInSeconds(seconds)
            .repeatForever()
    }

    /*
    * 静态方法创建对象, 每小时重复一次，次数不限制
    */
    public static func repeatHourlyForever(): SimpleScheduleBuilder {
        return simpleSchedule()
            .withIntervalInHours(1)
            .repeatForever()
    }

    /*
    * 静态方法创建对象, 每n小时重复一次，次数不限制
    */
    public static func repeatHourlyForever(hours: Int64): SimpleScheduleBuilder {
        return simpleSchedule()
            .withIntervalInHours(hours)
            .repeatForever()
    }

    /*
    * 静态方法创建对象, 每分钟重复一次， 触发总次数为count
    */
    public static func repeatMinutelyForTotalCount(count: Int64): SimpleScheduleBuilder {
        if(count < 1) {
            throw IllegalArgumentException("Total count of firings must be at least one! Given count: ${count}");
        }

        return simpleSchedule()
            .withIntervalInMinutes(1)
            .withRepeatCount(count - 1)
    }

    /*
    * 静态方法创建对象, 每n分钟重复一次， 触发总次数为count
    */
    public static func repeatMinutelyForTotalCount(count: Int64, minutes: Int64): SimpleScheduleBuilder {
        if(count < 1) {
            throw IllegalArgumentException("Total count of firings must be at least one! Given count: ${count}");
        }
        return simpleSchedule()
            .withIntervalInMinutes(minutes)
            .withRepeatCount(count - 1)
    }

    /*
    * 静态方法创建对象, 每秒重复一次， 触发总次数为count
    */
    public static func repeatSecondlyForTotalCount(count: Int64): SimpleScheduleBuilder {
        if(count < 1){
            throw IllegalArgumentException("Total count of firings must be at least one! Given count: ${count}");
        }
        return simpleSchedule()
            .withIntervalInSeconds(1)
            .withRepeatCount(count - 1)
    }

    /*
    * 静态方法创建对象, 每n秒重复一次， 触发总次数为count
    */
    public static func repeatSecondlyForTotalCount(count: Int64, seconds: Int64): SimpleScheduleBuilder {
        if(count < 1) {
            throw IllegalArgumentException("Total count of firings must be at least one! Given count: ${count}");
        }
        return simpleSchedule()
            .withIntervalInSeconds(seconds)
            .withRepeatCount(count - 1)
    }

    /*
    * 静态方法创建对象, 每小时重复一次， 触发总次数为count
    */
    public static func repeatHourlyForTotalCount(count: Int64): SimpleScheduleBuilder {
       if(count < 1) {
           throw IllegalArgumentException("Total count of firings must be at least one! Given count: ${count}");
       }
       return simpleSchedule()
           .withIntervalInHours(1)
           .withRepeatCount(count - 1)
    }

    /*
    * 静态方法创建对象, 每n小时重复一次， 触发总次数为count
    */
    public static func repeatHourlyForTotalCount(count: Int64, hours: Int64): SimpleScheduleBuilder {
       if(count < 1) {
           throw IllegalArgumentException("Total count of firings must be at least one! Given count: ${count}");
       }
       return simpleSchedule()
           .withIntervalInHours(hours)
           .withRepeatCount(count - 1)
    }

    /*
    * 设置间隔毫秒数
    */
    public func withIntervalInMilliseconds(intervalInMillis: Int64): SimpleScheduleBuilder {
        this.interval = intervalInMillis
        return this;
    }     

    /*
    * 设置间隔秒数
    */ 
    public func withIntervalInSeconds(intervalInSeconds: Int64 ): SimpleScheduleBuilder {
        this.interval = intervalInSeconds * 1000
        return this
    }

    /*
    * 设置间隔分钟数
    */ 
    public func withIntervalInMinutes(intervalInMinutes: Int64): SimpleScheduleBuilder {
        this.interval = intervalInMinutes * DateBuilder.MILLISECONDS_IN_MINUTE
        return this
    }

    /*
    * 设置间隔小时数
    */ 
    public func withIntervalInHours(intervalInHours: Int64): SimpleScheduleBuilder {
        this.interval = intervalInHours * DateBuilder.MILLISECONDS_IN_HOUR
        return this
    }

    /*
    * 设置重复次数
    */
    public func withRepeatCount(triggerRepeatCount: Int64): SimpleScheduleBuilder {
        this.repeatCount = triggerRepeatCount
        return this
    }

    /*
    * 设置不限次数
    */
    public func repeatForever(): SimpleScheduleBuilder {
        this.repeatCount = Trigger.REPEAT_INDEFINITELY;
        return this
    }    

    /*
    * 设置misfire策略为 ignore
    */
    public func withMisfireHandlingInstructionIgnoreMisfires(): SimpleScheduleBuilder {
        misfireInstruction = Trigger.MISFIRE_INSTRUCTION_IGNORE_MISFIRE_POLICY
        return this
    }    

    /*
    * 设置misfire策略为 fire now
    */
    public func withMisfireHandlingInstructionFireNow(): SimpleScheduleBuilder {
        misfireInstruction = SimpleTrigger.MISFIRE_INSTRUCTION_FIRE_NOW
        return this
    }    

    /*
    * 设置misfire策略为 MISFIRE_INSTRUCTION_RESCHEDULE_NEXT_WITH_EXISTING_COUNT
    */
    public func withMisfireHandlingInstructionNextWithExistingCount(): SimpleScheduleBuilder {
        misfireInstruction = SimpleTrigger.MISFIRE_INSTRUCTION_RESCHEDULE_NEXT_WITH_EXISTING_COUNT
        return this
    }

    /*
    * 设置misfire策略为 MISFIRE_INSTRUCTION_RESCHEDULE_NEXT_WITH_REMAINING_COUNT
    */
    public func withMisfireHandlingInstructionNextWithRemainingCount(): SimpleScheduleBuilder {
        misfireInstruction = SimpleTrigger.MISFIRE_INSTRUCTION_RESCHEDULE_NEXT_WITH_REMAINING_COUNT
        return this
    }

    /*
    * 设置misfire策略为 MISFIRE_INSTRUCTION_RESCHEDULE_NOW_WITH_EXISTING_REPEAT_COUNT
    */
    public func withMisfireHandlingInstructionNowWithExistingCount(): SimpleScheduleBuilder {
        misfireInstruction = SimpleTrigger.MISFIRE_INSTRUCTION_RESCHEDULE_NOW_WITH_EXISTING_REPEAT_COUNT
        return this
    }    

    /*
    * 设置misfire策略为 MISFIRE_INSTRUCTION_RESCHEDULE_NOW_WITH_REMAINING_REPEAT_COUNT
    */
    public func withMisfireHandlingInstructionNowWithRemainingCount(): SimpleScheduleBuilder {
        misfireInstruction = SimpleTrigger.MISFIRE_INSTRUCTION_RESCHEDULE_NOW_WITH_REMAINING_REPEAT_COUNT
        return this
    }    

    /*
    * 构造最终的 Trigger 对象
    */
    public override func build(): MutableTrigger {
       let t = SimpleTriggerImpl()
       t.setRepeatInterval(interval)
       t.setRepeatCount(repeatCount)
       t.setMisfireInstruction(misfireInstruction)
       return t
    }

}