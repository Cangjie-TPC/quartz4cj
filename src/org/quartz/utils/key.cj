/*
 * 版权所有 (c) 上海赛可出行科技服务有限公司 2023-2023
 * 
 * Created by ranpengfei on 2023/08/04
 */
package quartz.org.quartz.utils

import std.time.*

public let DEFAULT_GROUP = "DEFAULT"

/*
 * JobKey和TriggerKey的基类
 */
public open class Key<T> <: ToString & Hashable & Equatable<T> where T <: Key<T> {

    let name: String
    let group: String

    public Key(name: String, group: String) {
        this.name = name
        this.group = group
    }

    public init(name: String) {
        this.name = name
        this.group = DEFAULT_GROUP
    }

    /*
    * 获取 name 
    */
    public func getName(): String {
        return this.name;
    }

    /*
    * 获取 group 
    */
    public func getGroup(): String {
        return group
    }

    public open func toString(): String {
        return group + "." + name
    }

    public override func hashCode(): Int64 {
        let prime: Int64 = 31
        var result: Int64 = 1
        result = prime * result + group.hashCode()
        result = prime * result + name.hashCode()
        return result
    }

    public operator func ==(o: T): Bool {
        return group == o.group && name == o.name
    }

    public func compare(o: T): Ordering {
        if (group == DEFAULT_GROUP && o.group != DEFAULT_GROUP) {
            return Ordering.LT;
        }

        if (group != DEFAULT_GROUP && o.group == DEFAULT_GROUP) {
            return Ordering.GT;
        }

        let r = group.compare(o.group)
        if (r != Ordering.EQ) {
            return r
        }

        return name.compare(o.getName());
    }

    public operator func !=(o:  T): Bool {
        return !(this == o)
    }

    public operator func <(o:  T): Bool {
        return this.compare(o) == Ordering.LT
    }

    public operator func >(o:  T): Bool {
        return this.compare(o) == Ordering.GT
    }

    public operator func <=(o:  T): Bool {
        return match (this.compare(o)) {
            case LT | EQ => true
            case _ => false
        }
    }

    public operator func >=(o:  T): Bool {
        return match (this.compare(o)) {
            case GT | EQ => true
            case _ => false
        }
    }

    public static func createUniqueName(group!: String = DEFAULT_GROUP): String {

        // String n1 = UUID.randomUUID().toString();
        // String n2 = UUID.nameUUIDFromBytes(group.getBytes()).toString();
        // return String.format("%s-%s", n2.substring(24), n1);

        /*
        sample output:

        4934ztey-gm8beu06cf
        4934ztey-gm8beu075t
        4934ztey-gm8beu07qe
        48yhvg5n-gm8beu08c9
        48yhvg5n-gm8beu08ur
        48yhvg5n-gm8beu0995

        */
        let s1 = randomStr();
        let s2 = randomStr(group)
        let s3 = if( s2.size > 8 ) { substring(s2,s2.size-8) } else { s2 }
        return "${s3}-${s1}"
    }

    private static func randomStr(): String {
        let c = DateTime.now().toUnixTimeStamp().toMicroseconds()
        return toRadixString(c,36)
    }

    private static func randomStr(s:String): String {
        return toRadixString(0x80000000000 + s.hashCode(),36)
    }

}
