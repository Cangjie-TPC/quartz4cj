/*
 * 版权所有 (c) 上海赛可出行科技服务有限公司 2023-2023
 * 
 * Created by ranpengfei on 2023/08/04
 */
package quartz.org.quartz.utils

import std.unittest.*
import std.unittest.testmacro.*
import std.collection.*

class MyKey <: Key<MyKey>  {
    public init(name: String) {
        super(name);
    }

    public init(name: String, group: String) {
        super(name, group);
    }

}

@Test
public class MyKeyTest {
    @TestCase
    public func testBasic1(): Unit {
        let a = MyKey("abc")
        @Assert( "abc", a.getName())
        @Assert( DEFAULT_GROUP, a.getGroup() )
        println( "hashCode=${a.hashCode()}" )

        let b = MyKey("abc","123")
        @Assert( "abc", b.getName() )
        @Assert( "123", b.getGroup() )
        @Assert( "123.abc", b.toString() )
    }

    @TestCase
    public func testBasic2(): Unit {
        let a = MyKey("abc")
        let b = MyKey("abc","123")
        @Assert( a < b ) 
        @Assert( a <= b ) 
        @Assert( a != b ) 
        @Assert( false, a == b ) 
        @Assert( false, a >= b ) 
        @Assert( false, b <= a ) 
        @Assert( false, a > b ) 
        @Assert( Ordering.LT, a.compare(b) ) 
        @Assert( Ordering.GT, b.compare(a) ) 

        let a2 = MyKey("abc","test1")
        let b2 = MyKey("abc","test2")
        @Assert( Ordering.EQ != a2.compare(b2) ) 

        let a3 = MyKey("abc","test1")
        let b3 = MyKey("def","test1")
        @Assert( Ordering.EQ != a3.compare(b3) ) 
    }
 
 
    @TestCase
    public func testUniqueName(): Unit {
        let k1 = MyKey.createUniqueName()
        println("k1=${k1}")
        let k2 = MyKey.createUniqueName(group: "testgroup")
        println("k2=${k2}")
    }

}
