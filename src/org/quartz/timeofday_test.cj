/*
 * 版权所有 (c) 上海赛可出行科技服务有限公司 2023-2023
 * 
 * Created by xujian on 2023/08/04
 */
package quartz.org.quartz

import std.unittest.*
import std.unittest.testmacro.*
import std.time.*

@Test
public class TimeOfDayTest {

    @TestCase
    public func case1(): Unit {
        var t = TimeOfDay(12,12)     
        @Assert( 12, t.getHour() )
        @Assert( 12, t.getMinute() )
        @Assert( 0, t.getSecond() )
        t = TimeOfDay.hourAndMinuteOfDay(12,12)     
        @Assert( 12, t.getHour() )
        @Assert( 12, t.getMinute() )
        @Assert( 0, t.getSecond() )
        t = TimeOfDay(11,11,11)     
        @Assert( 11, t.getHour() )
        @Assert( 11, t.getMinute() )
        @Assert( 11, t.getSecond() )
        t = TimeOfDay.hourMinuteAndSecondOfDay(11,11,11)     
        @Assert( 11, t.getHour() )
        @Assert( 11, t.getMinute() )
        @Assert( 11, t.getSecond() )
    }

    @TestCase
    public func case2(): Unit {
        var t1 = TimeOfDay(12,12)    
        var t2 = TimeOfDay(12,13)     
        @Assert( true, t1.before(t2) ) 
        t2 = TimeOfDay(12,11)     
        @Assert( false, t1.before(t2) )
        t2 = TimeOfDay(13,1)     
        @Assert( true, t1.before(t2) )
        t2 = TimeOfDay(11,50)     
        @Assert( false, t1.before(t2) )

        t1 = TimeOfDay(12,12,12)    
        t2 = TimeOfDay(12,12,13)   
        @Assert( true, t1.before(t2) )
        t2 = TimeOfDay(12,12,11)   
        @Assert( false , t1.before(t2) ) 
    }

    @TestCase
    public func case3(): Unit {

        let dt = DateTime.of(year: 2023,month: 7,dayOfMonth: 15, 
                hour: 12, minute: 0, second: 0, nanosecond: 0 )

        var t1 = TimeOfDay(12,12,12)    

        let dt2 = t1.getTimeOfDayForDate(dt)
        @Assert( 12 , dt2.hour ) 
        @Assert( 12 , dt2.minute ) 
        @Assert( 12 , dt2.second ) 
        @Assert( 0 , dt2.nanosecond ) 
    }

    @TestCase
    public func case4(): Unit {

        let dt = DateTime.of(year: 2023,month: 7,dayOfMonth: 15, 
                hour: 12, minute: 12, second: 12, nanosecond: 1000 )

        let t = TimeOfDay.hourAndMinuteAndSecondFromDate(dt)  
        @Assert( 12, t.getHour() )
        @Assert( 12, t.getMinute() )
        @Assert( 12, t.getSecond() )

        println("t=${t}")

        let t2 = TimeOfDay.hourAndMinuteAndSecondFromDate(dt,TimeZone.Local)  
        @Assert( 12, t2.getHour() )
        @Assert( 12, t2.getMinute() )
        @Assert( 12, t2.getSecond() )
        println("t=${t}")

        let dt3 = DateTime.of(year: 2023,month: 7,dayOfMonth: 15, 
                hour: 12, minute: 12, second: 12, nanosecond: 1000, timeZone: TimeZone.load("Asia/Shanghai") )
        let t3 = TimeOfDay.hourAndMinuteAndSecondFromDate(dt3,TimeZone.UTC)  
        @Assert( 4, t3.getHour() )
        @Assert( 12, t3.getMinute() )
        @Assert( 12, t3.getSecond() )


    }

    @TestCase
    public func case5(): Unit {

        let dt = DateTime.of(year: 2023,month: 7,dayOfMonth: 15, 
                hour: 12, minute: 12, second: 12, nanosecond: 1000 )

        let t = TimeOfDay.hourAndMinuteFromDate(dt)  
        @Assert( 12, t.getHour() )
        @Assert( 12, t.getMinute() )
        @Assert( 0, t.getSecond() )

        println("t=${t}")

        let t2 = TimeOfDay.hourAndMinuteFromDate(dt,TimeZone.Local)  
        @Assert( 12, t2.getHour() )
        @Assert( 12, t2.getMinute() )
        @Assert( 0, t2.getSecond() )
        println("t=${t}")

        let dt3 = DateTime.of(year: 2023,month: 7,dayOfMonth: 15, 
                hour: 12, minute: 12, second: 12, nanosecond: 1000, timeZone: TimeZone.load("Asia/Shanghai") )
        let t3 = TimeOfDay.hourAndMinuteFromDate(dt3,TimeZone.UTC)  
        @Assert( 4, t3.getHour() )
        @Assert( 12, t3.getMinute() )
        @Assert( 0, t3.getSecond() )


    }

    @TestCase
    public func case6(): Unit {
        let t1 = TimeOfDay(12,10,10)
        let t2 = TimeOfDay(13,10,10)
        let t3 = TimeOfDay(12,10,10)

        @Assert( true, t1 < t2 )
        @Assert( true, t1 <= t2 )
        @Assert( true, t1 == t3 )
        @Assert( true, t2 > t1 )
        @Assert( true, t2 >= t1 )
        @Assert( true, t2 != t1 )
        @Assert( true, t1.compare(t2) == LT )
        @Assert( true, t1.before(t2) )

        @Assert( 0, TimeOfDay(0,0,0).hashCode() )
        @Assert( 3661, TimeOfDay(1,1,1).hashCode() )

    }

}


