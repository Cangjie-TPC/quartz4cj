/*
 * 版权所有 (c) 上海赛可出行科技服务有限公司 2023-2023
 * 
 * Created by ranpengfei on 2023/08/04
 */
package quartz.org.quartz

import std.log.*
import std.console.*

import quartz.org.quartz.utils.*

public open class SimpleJobFactory <: JobFactory {

    private static let log = LoggerFactory.getLogger("org.quartz.SimpleJobFactory")

    public open func newJob(bundle: TriggerFiredBundle, scheduler: Scheduler) : Job {

        let jobDetail = bundle.getJobDetail()
        let jobClass = jobDetail.getJobClass()
        try {
            if( log.isDebugEnabled()) {
                log.debug("Producing instance of Job '${jobDetail.getKey()}', class=${jobClass.getClass()}")
            }
            return jobClass.newInstance();
        } catch (e: Exception) {
            throw SchedulerException( "Problem instantiating class '${jobDetail.getJobClass().getClass()}'" + e.message )
        }
    }

}