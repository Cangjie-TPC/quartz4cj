/*
 * 版权所有 (c) 上海赛可出行科技服务有限公司 2023-2023
 * 
 * Created by ranpengfei on 2023/08/04
 */
package quartz.org.quartz

import quartz.org.quartz.utils.*

/*
 *  NotMatcher 可以将Matcher的结果取反
 */
public class NotMatcher<T> <: Matcher<T> where T <: Key<T> {
       
    protected NotMatcher(let operand: Matcher<T>) {
    }

    /*
    * 创建对象
    */
    public static func not(operand: Matcher<T>) : NotMatcher<T> {
        return NotMatcher(operand);
    }

    /*
    * 传入的 JobKey 或 TriggerKey 是否匹配
    */
    public func isMatch(key: T) : Bool {
        return !operand.isMatch(key);
    }

    public func getOperand() : Matcher<T> {
        return operand;
    }

    public func hashCode() : Int64 {
        let prime = 31;
        var result = 1;
        result = prime * result + operand.hashCode();
        return result;
    }
 
    public operator func ==(obj: Matcher<T>) : Bool {
        match( obj ) {
            case other: NotMatcher<T> => 
                return this.operand == other.operand
            case _ => false
        }
    }

    public operator func !=(obj: Matcher<T>) : Bool {
        match( obj ) {
            case other: NotMatcher<T> => 
                return this.operand != other.operand
            case _ => true
        }
    }

}
